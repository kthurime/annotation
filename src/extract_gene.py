import argparse
from Bio import SeqIO


def extract_nr_genes(seqs, gene_list, output):
    gene_set = set(line.strip() for line in open(gene_list, 'r'))

    fasta_sequences = SeqIO.parse(open(seqs), 'fasta')

    with open(output, 'w') as f:
        for fasta in fasta_sequences:
            if str(fasta.id) in gene_set:
                f.write(">" + fasta.description + "\n" + str(fasta.seq) + "\n")


def main(seqs, gene_list, output):
    extract_nr_genes(seqs, gene_list, output)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # Primary inputs
    parser.add_argument('-s', '--sequences', help="Protein Sequences",
                        required=True)
    parser.add_argument('-g', '--genes', help="Unique Gene List",
                        required=True)
    parser.add_argument('-o', '--output', help="Output Non Redudent File",
                        required=True)

    args = parser.parse_args()
    seqs = args.sequences
    output = args.output
    genes = args.genes

    main(seqs, genes, output)
