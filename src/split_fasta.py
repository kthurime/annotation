#!/bin/env python

from optparse import OptionParser

from Bio import SeqIO

parser = OptionParser()
parser.add_option("-f", "--fasta", dest="fasta_in", help="gene catalog fasta file", metavar="FILENAME")
parser.add_option("-p", "--prefix", dest="prefix_file_in", help="prefix", metavar="FILENAME")
parser.add_option("-o", "--folder.out", dest="folder_out", help="output_folder", metavar="FILENAME")
parser.add_option("-l", "--length", dest="length", help="length", metavar="LENGTH")

(options, args) = parser.parse_args()

print(options)
print(args)

fastaFile = options.fasta_in
folderOUT = options.folder_out
prefixFileOUT = options.prefix_file_in
length = int(options.length)

print(fastaFile)
print(folderOUT)
print(prefixFileOUT)
print(length)

i = 1  # file counter
j = 0  # sequence counter
fileout = open(folderOUT + "/" + prefixFileOUT + "_" + str(i) + ".fa", 'w')
with open(fastaFile, 'rU') as handle:
    for record in SeqIO.parse(handle, "fasta"):
        j = j + 1
        if j % length == 0:
            i = i + 1
            fileout.close()
            fileout = open(folderOUT + "/" + prefixFileOUT + "_" + str(i) + ".fa", 'w')
            print(j, ".. parsed")
        fileout.write(">" + str(record.description) + "\n" + str(record.seq) + "\n")
fileout.close()
print("Finished parsing fasta.", i, "files generated")