# Use an official Python runtime as a parent image
FROM ubuntu

ENV DEBIAN_FRONTEND noninteractive

# Prepare directory structure
RUN mkdir /app && mkdir /appdownload

ENV PATH /app:$PATH

COPY src/extract_gene.py /app

COPY src/split_fasta.py /app

RUN apt-get update && apt-get install -y \
  python \
  python3-pip \
  r-base \
  r-cran-curl \
  r-cran-openssl \
  r-cran-xml2 \
  libxml2-dev

## copy files
COPY src/install_packages.R /app/install_packages.R
COPY src/requirements.txt /app/requirements.txt

## install R-packages
RUN Rscript /app/install_packages.R

COPY src/geneCatalog_annotate.R /app/geneCatalog_annotate.R
COPY src/make_msp_taxonomy_report.R /app/make_msp_taxonomy_report.R
COPY src/server_kegg_annotation.R /app/server_kegg_annotation.R

RUN pip install --no-cache-dir --upgrade pip && pip install --no-cache-dir -r /app/requirements.txt

RUN chmod 755 /app/*